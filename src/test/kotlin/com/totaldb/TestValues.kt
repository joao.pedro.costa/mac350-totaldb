package com.totaldb

import tw_models.abilities.BoundedAbility
import tw_models.abilities.LoreOfMagic
import tw_models.abilities.Spell
import tw_models.factions.Race
import tw_models.factions.Faction
import tw_models.units.ArmyUnit
import tw_models.units.GenericLord
import tw_models.units.Hero
import tw_models.units.LordData
public val kislev = Race("Kislev")
public val dwai = Race("Dwarfs")
public val empire = Race("Empire")
public val helfs = Race("High Elves")
public val iceCourt = Faction("The Ice Court", race = kislev.name, isMajor = true)

public val iceMagic = LoreOfMagic(name = "Lore of Ice", availableFactions = listOf(kislev.name))
public val lifeMagic = LoreOfMagic(name = "Lore of Life", availableFactions = listOf(helfs.name))
public val miscSpells = LoreOfMagic(name = "None", availableFactions = listOf(kislev.name, helfs.name))

public val iceSheet = Spell(
    name = "Ice Sheet",
    ablType = "Hex(Area)",
    useCount = -1,
    duration = 21,
    cooldown = 27,
    range = 200,
    effectRange = 55,
    effects = listOf("Speed -25%", "Acceleration -25%", "Charge Speed -25%"),
    affectedUnits = "Affects enemies in range(all)",
    affectsIf = "Grounded",
    magicLore = iceMagic.name,
    windsCost = 4,
    overcastCost = -1,
    targets = listOf("Ground", "Enemies"),
    owner = "global"
)
public val frostBlades = Spell(
    name = "Frost Blades",
    ablType = "Augment",
    useCount = -1,
    duration = 26,
    cooldown = 43,
    effects = listOf("Base Weapon Damage +25%", "Armor-Piercing Weapon Damage +25%", "Melle Attack +24", "Imbuement: Magical Attacks"),
    affectedUnits = "Affects allies in range(max:1)",
    range = 200,
    magicLore = iceMagic.name,
    targets = listOf("Self", "Ally"),
    windsCost = 7,
    overcastCost = 10,
    overcastEffects = listOf("Base Weapon Damage +50%", "Armor-Piercing Weapon Damage +50%", "Melle Attack +24", "Imbuement: Magical Attacks"),
    owner = "global"
)
public val earthBlood = Spell(
    name = "Earth Blood",
    ablType = "Regeneration(Area)",
    useCount = -1,
    duration = 7,
    cooldown = 30,
    effects = listOf("Heal Per Second: +0.80%"),
    affectedUnits = "Affects allies in range(max:4)",
    affectsIf = "Hit point replenishment cap not reached",
    range = 200,
    effectRange = 35,
    magicLore = lifeMagic.name,
    targets = listOf("Ground", "Self", "Allies"),
    windsCost = 6,
    overcastCost = 11,
    overcastEffects = listOf("Duration: 14"),
    owner = "global"
)
public val kisWarrior = ArmyUnit(name = "Kislevite Warriors", owner = "global")

public val katarin  = LordData(
    name = "Tzaarina Katarin",
    charType = "Wizard",
    isPlayable = true,
    isLeg = true,
    race = kislev.name,
    faction = iceCourt.name,
    startingArmy = listOf(kisWarrior.name, kisWarrior.name),
    health = 3888,
    leadership = 80,
    speed = 46,
    meleeAttack = 50,
    meleeDefense = 45,
    chargeBonus = 20,
    meleeWeaponStrength = 400,
    meleeBaseDamage = 260,
    meleeAP = 140,
    meleeInterval = 4.0,
    meleeReach = 3,
    armor = 15,
    costMP = 550,
    upkeep = 300,
    weightClass = "Heavy",
    gameID = "wh3_main_ksl_cha_katarin_0",
    mountList = listOf("Warhorse", "War Bear", "Frost Wyrm", "Ice Court Sled"),
    abilityList = listOf("Ice Maiden's Kiss", "Ice Sheet", "Guardian Call","Stand Your Ground!","Arcane Conduit","Frost Shield", "Frost Fang"),
    hasSpells = true,
    spellsMap = mapOf(iceMagic.name to listOf(iceSheet.name, frostBlades.name)),
    owner = "global",
    hasRanged = false,

)
public val iceWitchIce  = GenericLord(
    name = "Ice Witch (Ice)",
    charType = "Wizard",
    race = kislev.name,
    health = 3680,
    leadership = 65,
    speed = 38,
    meleeAttack = 40,
    meleeDefense = 35,
    chargeBonus = 20,
    meleeWeaponStrength = 320,
    meleeBaseDamage = 220,
    meleeAP = 100,
    meleeInterval = 4.0,
    meleeReach = 3,
    meleeVsBuilding = 1.01,
    armor = 15,
    costMP = 400,
    upkeep = 250,
    weightClass = "Heavy",
    gameID = "wh3_main_ksl_cha_ice_witch_ice_0",
    mountList = listOf("Warhorse", "War Bear", "Frost Wyrm"),
    abilityList = listOf("Guardian Call", "Arcane Conduit","Frost Shield","Glacial Blast","Evasion"),
    owner = "global"

)
public val ulrika = Hero(
    name = "Ulrika Magdova",
    raceList = listOf(kislev.name, dwai.name, empire.name),
    isLeg = true,
    recruitType = "dilemma",
    recruitSource = listOf("Ulrika the Vampire"),
    charType = "Hybrid Weapon Wizard",
    health = 3968,
    leadership = 80,
    speed = 40,
    meleeAttack = 50,
    meleeDefense = 46,
    chargeBonus = 25,
    meleeWeaponStrength = 450,
    meleeAP = 200,
    meleeBaseDamage = 250,
    meleeInterval = 4.0,
    meleeReach = 3,
    meleeVsBuilding = 1.01,
    hasRanged = true,
    rangedMissileStrength = 297,
    rangedBaseDamage = 124,
    rangedAp = 84,
    rangedProjNum = 1,
    rangedVsBuilding = 1.01,
    reload = 7,
    rangedRange = 180,
    armor = 50,
    gameID = "wh3_dlc23_new_cha_ulrika",
    weightClass = "Heavy",
    costMP = 950,
    upkeep = 150,
    missileBlock = 0,
    abilityList = listOf("The Withering","Pit of Shades","Skjalandir's Fall","The Dancing Blade", "Crumbling", "Desintegration", "The Hunger", "Smoke and Mirrors"),
    mountList = listOf("Barded Warhorse"),
    owner = "global"
)
public val patriarch = Hero(
    name = "Patriarch",
    raceList = listOf(kislev.name),
    isLeg = false,
    recruitType = "building",
    recruitSource = listOf("Orthodoxy Church", "Orthodoxy Chapel", "Magnus Gardens"),
    charType = "Patriarch",
    health = 3420,
    leadership = 70,
    speed = 34,
    meleeAttack = 35,
    meleeDefense = 30,
    meleeWeaponStrength = 360,
    meleeBaseDamage = 240,
    meleeAP = 120,
    meleeInterval = 3.6,
    meleeReach = 3,
    meleeVsBuilding = 1.25,
    armor = 20,
    gameID = "wh3_main_ksl_cha_patriarch_0",
    weightClass = "Medium",
    upkeep = 250,
    costMP = 350,
    missileBlock = 0,
    abilityList = listOf("Ursun's Roar", "Dazh's Song of Winter Sunlight", "Salyak Lullaby", "Tor's Battle Hymn", "The Courage of Sacrifie"),
    mountList = listOf("Warhorse", "Warbear"),
    owner = "global"
)
public val kisleviteWarriors = ArmyUnit(
    name = "Kislevite Warriors",
    recruitTypes = listOf("building"),
    recruitSources = listOf("Village", "Village (Minor)", "Erengrad (Village)", "Kislev (Nascent City)", "Praag (Cursed City)"),
    race = kislev,
    charType = "melee infantry",
    health = 8400,
    leadership = 60,
    speed = 33,
    meleeAttack = 24,
    meleeDefense = 40,
    chargeBonus = 8,
    meleeWeaponStrength = 28,
    meleeBaseDamage = 8,
    meleeAP = 20,
    meleeInterval = 4.4,
    meleeReach = 1,
    armor = 30,
    gameID = "wh3_dlc24_ksl_inf_kislevite_warriors",
    upkeep = 125,
    weightClass = "Medium",
    costMP = 500,
    unitCountSmall = 30,
    unitCountMedium = 60,
    unitCountLarge = 90,
    unitCountUltra = 120,
    abilityList = listOf("Charge Reflection"),
    owner = "global"
)
public val boyar: LordData = LordData(
    name = "Boyar",
    race = kislev.name,
    faction = "none",
    hasSpells = false,
    isLeg = false,
    isPlayable = true,
    spellsMap = mutableMapOf(),
    startingArmy = listOf(),
    mountList = listOf("Warhorse", "Warbear"),
    charType = "Axe Infantry",
    health = 4060,
    leadership = 70,
    speed = 35,
    meleeAttack = 50,
    meleeDefense = 40,
    chargeBonus = 45,
    meleeWeaponStrength = 430,
    meleeBaseDamage = 290,
    meleeAP = 140,
    meleeInterval = 4.0,
    meleeReach = 3,
    meleeVsBuilding = 1.25,
    hasRanged = false,
    armor = 85,
    gameID = "wh3_main_ksl_cha_boyar_0",
    upkeep = 250,
    weightClass = "Medium",
    costMP = 750,
    missileBlock = 0,
    abilityList = listOf("Foe-Seeker", "Deadly Onslaught", "Heroic Resilience"),
    owner = "global"
)