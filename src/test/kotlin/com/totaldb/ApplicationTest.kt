package com.totaldb

import com.google.gson.Gson
import com.totaldb.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.testing.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import mongoConnection.heroesColletion
import mongoConnection.lordsCollection
//import mongoConnection.insertToMongo
import com.mongodb.client.model.Filters.*
import com.mongodb.client.model.Filters
import com.mongodb.client.model.UpdateOptions
import com.mongodb.client.model.Updates
import com.typesafe.config.ConfigException.Generic
import com.typesafe.config.ConfigException.Null
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.encodeToJsonElement
import mongoConnection.*
import tw_models.abilities.LoreData
import tw_models.abilities.LoreOfMagic
import kotlin.test.*

import tw_models.factions.*
import tw_models.units.*
import java.util.logging.Filter

class ApplicationTest {
//    @Test
//    fun testRoot() = testApplication {
//        application {
//            configureRouting()
//        }
//        client.get("/").apply {
//            assertEquals(HttpStatusCode.OK, status)
//            assertEquals("Hello World!", bodyAsText())
//        }
//    }
    @Test
    fun testMainFaction() = testApplication {
        application {
//            configureSerialization()
            configureRouting()
        }
        client.get(urlString = "/").apply {
            assertEquals(HttpStatusCode.OK, status)
            val legFactions: List<String> = listOf("The Ice Court","The Great Orthodoxy","Ursun Revivalists", "Daughters of the Forest", "Kislev Expedition")
            val minFactions: List<String> = listOf("Brotherhood of the Bear", "Druzhina Enclave", "Kislev Rebels", "Ropsmenn Clan", "Ungol Kindred")

            val kislev = Race("Kislev")
            for (faction in legFactions) {
                val newFaction = Faction(name = faction, race = kislev.name, isMajor = true)
            }
            for (faction in minFactions) {
                val newFaction = Faction(name = faction, race = kislev.name, isMajor = false)
            }

            val kislevLegFactions = kislev.getMajorFactions()
            val kislevMinFactions = kislev.getMinorFactions()

            for (i in kislevLegFactions.indices) {
                assertEquals(legFactions[i],kislevLegFactions[i].name)
            }
            for (i in kislevMinFactions.indices) {
                assertEquals(minFactions[i],kislevMinFactions[i].name)
            }
            println("All tests concluded successfully!")
        }
    }
    @Test
    fun testSubFaction() = testApplication {
        application {
            configureRouting()
        }

        client.get(urlString = "/").apply {
            assertEquals(HttpStatusCode.OK, status)
            val kostaltyn = LordData(name = "Kostaltyn", owner = "global")
            iceCourt.startingLord = katarin

            assertEquals(iceCourt.startingLord, katarin)
            iceCourt.startingLord = kostaltyn
            assertEquals(iceCourt.startingLord, katarin)

        }
    }

    @Test
    fun testLegendaryLord() = testApplication {
        application {
            configureRouting()
        }
        client.get(urlString = "/").apply {
            assertEquals(HttpStatusCode.OK, status)
            val startArmy: List<ArmyUnit> = listOf(kisWarrior, kisWarrior)

            assertEquals(katarin.race, "Kislev")
            assertNotNull(katarin.faction)
            assertEquals(katarin.faction, "The Ice Court")
            val katArmy = katarin.startingArmy
            assertNotNull(katArmy)
            assertEquals(katArmy.size, 2)
            for (unit in katArmy){
                assertEquals(unit, "Kislevite Warriors")
            }
            val katMounts = katarin.mountList
            assertNotNull(katMounts)
            assertEquals(katMounts.size, 4)
            assertEquals(katarin.isLeg, true)
            assertEquals(katarin.unitCountUltra,1)
            val katAbil = katarin.abilityList
            assertNotNull(katAbil)
            assertEquals(katAbil.size, 7)
        }
    }
    @Test
    fun testGenericLord() = testApplication {
        application {
            configureRouting()
        }
        client.get(urlString = "/").apply {
            assertEquals(HttpStatusCode.OK, status)
            val kislev = Race("Kislev")

            assertEquals(iceWitchIce.race, "Kislev")
            val iceMounts = iceWitchIce.mountList
            assertNotNull(iceMounts)
            assertEquals(iceMounts.size, 3)
            assertEquals(iceWitchIce.isLeg, false)
            assertEquals(iceWitchIce.unitCountUltra,1)
            val iceAbil = iceWitchIce.abilityList
            assertNotNull(iceAbil)
            assertEquals(iceAbil.size, 5)
        }
    }

    @Test
    fun testHero() = testApplication {
        application {
            configureRouting()
        }

        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)

            assertEquals(ulrika.name, "Ulrika Magdova")
            assertEquals(ulrika.unitCountUltra,1)
            assertEquals(ulrika.isLeg, true)
            assertEquals(ulrika.raceList.size, 3)
            assertContentEquals(ulrika.raceList, listOf(kislev.name, dwai.name, empire.name))

            assertEquals(patriarch.isLeg, false)
            assertEquals(patriarch.recruitType, "building")
            assertContentEquals(patriarch.recruitSource, listOf("Orthodoxy Church", "Orthodoxy Chapel", "Magnus Gardens"))
            assertEquals(patriarch.abilityList[0], "Ursun's Roar")

        }
    }

    @Test
    fun testArmyUnit() = testApplication {
        application{
            configureRouting()
        }

        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)

            assertEquals(kisleviteWarriors.unitCountUltra, 120)
            assertEquals(kisleviteWarriors.name, "Kislevite Warriors")
            assertContentEquals(kisleviteWarriors.recruitSources, listOf("Village", "Village (Minor)", "Erengrad (Village)", "Kislev (Nascent City)", "Praag (Cursed City)"))
            assertEquals(kisleviteWarriors.abilityList[0], "Charge Reflection")
        }
    }

    @Test
    fun testInsertToMongo() = testApplication {
        application {
            configureRouting()
        }
        val katOnMongo = lordsCollection.find<LordData>(eq("name", katarin.name)).firstOrNull()
        if (katOnMongo == null) {
            lordsCollection.insertOne(katarin)
        } else {
            lordsCollection.replaceOne(eq("name", katarin.name), katarin)
        }
        val patriOnMongo = heroesColletion.find<Hero>(eq("name", patriarch.name)).firstOrNull()
        if (patriOnMongo == null) {
            heroesColletion.insertOne(patriarch)
        }
        else {
            heroesColletion.replaceOne(eq("name", patriarch.name), patriarch)
        }
    }

        @Test
    fun testGetFromMongo() = testApplication {
        application {
            configureRouting()
        }

        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)
//          construção de objetos do tipo lord
            val katarin = lordsCollection.find<LordData>(eq("name","Tzaarina Katarin")).firstOrNull()
            val iceWitch = lordsCollection.find<LordData>(eq("name","Ice Witch (Ice)")).firstOrNull()
            val errLord = lordsCollection.find<LordData>(eq("name","invalid")).firstOrNull()
            assertNotNull(katarin)
            assertEquals("Tzaarina Katarin", katarin.name)
            assertTrue(katarin.isLeg)
            val loreIce = loresCollection.find<LoreData>(eq("name", "Lore of Ice")).firstOrNull()
            val katSpells = katarin.spellsMap
            assertNotNull(katSpells)
            assertEquals(1, katSpells.keys.size)
            assertTrue(katSpells.keys.any{it == "Lore of Ice"})
            val lore = katSpells.keys.firstOrNull{it == "Lore of Ice"}
            val iceSpells = katSpells[lore]
            assertNotNull(iceSpells)
            assertEquals(2, iceSpells.size)
            assertEquals("Ice Sheet", iceSpells[0])
            assertEquals("Frost Blades", iceSpells[1])
            assertNotNull(iceWitch)
            assertEquals("Ice Witch (Ice)", iceWitch.name)
            assertEquals("LordData", iceWitch::class.simpleName)
            assertFalse(iceWitch.isLeg)
            val lordList = lordsCollection.find(Filters.`in`("name",listOf("Tzaarina Katarin", "Ice Witch (Ice)", "invalid"))).toList()


            assertNotNull(lordList)
            val teste0 = lordList[0]
            assertNotNull(teste0)
            assertEquals(katarin.name, teste0.name)
            val teste1 = lordList[1]
            assertNotNull(teste1)
            assertEquals(iceWitch.name, teste1.name)

        }
    }

    @Test
    fun testSpells() = testApplication {
        application {
            configureRouting()
        }

        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)
            val loremaster = Hero("Loremaster of Hoeth", raceList = listOf(helfs.name), hasSpells = true,
                spellsMap = mutableMapOf(
                    miscSpells.name to mutableListOf(frostBlades.name, earthBlood.name),
                    iceMagic.name to mutableListOf(iceSheet.name)
                ))
//            katarin.addSpell(iceSheet.name, iceMagic.name)
////            katarin.addSpell(frostBlades.name, iceMagic.name)
//            loremaster.addSpell(iceSheet.name, iceMagic.name)
//            loremaster.addSpell(frostBlades.name, miscSpells.name)
//            loremaster.addSpell(earthBlood.name, miscSpells.name)
//            try {
//                patriarch.addSpell(earthBlood.name, lifeMagic.name)
//            } catch (e: Exception) {
//                assertEquals(e.message, "Patriarch can not cast spells")
//            }
            val katarinSpells = mutableMapOf(
                iceMagic.name to mutableListOf(iceSheet.name, frostBlades.name)
            )
            val loremasterSpells = mutableMapOf(
                iceMagic.name to mutableListOf(iceSheet.name),
                miscSpells.name to mutableListOf(frostBlades.name, earthBlood.name)
            )
            val katSpells = katarin.spellsMap
            assertNotNull(katSpells)
            assertEquals(katSpells.keys, katarinSpells.keys)
            for (lore in katSpells.keys) {
                assertContentEquals(katSpells[lore], katarinSpells[lore])
            }
            val loreSpells = loremaster.spellsMap
            assertNotNull(loreSpells)
            assertEquals(loreSpells.keys, loremasterSpells.keys)
            for (lore in loreSpells.keys) {
                assertContentEquals(loreSpells[lore], loremasterSpells[lore])
            }
            assert(patriarch.spellsMap.isEmpty())


        }
    }

    @Test
    fun testLordGet() = testApplication {
        application {
            configureRouting()
        }
        val response = client.get("/db/katarin")
        assertEquals(HttpStatusCode.OK, response.status)
        assertEquals("Tzaarina Katarin", response.bodyAsText())
    }

    @Test
    fun testLordPOST() = testApplication {
        application {
            configureRouting()
        }
        val response = client.post("/db/addLord"){
            contentType(ContentType.Application.Json)
            setBody(Json.encodeToString(katarin))
        }
        val body = Json.encodeToJsonElement(katarin)
        println(body)
        assertEquals(HttpStatusCode.OK, response.status)

    }
}


