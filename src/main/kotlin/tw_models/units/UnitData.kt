package tw_models.units
import kotlinx.serialization.Serializable
import mongoConnection.MongoData

@Serializable
data class UnitData (
    val id: String?,
    override val name: String,
    val charType: String?,
    val isPlayable: Boolean?,
    val race: String?,
    val faction: String?,
    val health: Int?,
    val leadership: Int?,
    val speed: Int?,
    val meleeAttack: Int?,
    val meleeDefense: Int?,
    val chargeBonus: Int?,
    val meleeWeaponStrength: Int?,
    val meleeAP: Int?,
    val meleeInterval: Double?,
    val meleeReach: Int?,
    val costMP: Int?,
    val upkeep: Int?,
    val weightClass: String?,
    val gameID: String?,
    val mountList: List<String>?,
    val abilityList: List<String>?,
    val unitCountSmall: Int?,
    val unitCountMedium: Int?,
    val unitCountLarge: Int?,
    val unitCountUltra: Int?,
    val owner: String?
) : MongoData()