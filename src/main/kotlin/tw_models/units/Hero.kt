package tw_models.units

import kotlinx.coroutines.runBlocking
import tw_models.factions.Race
import kotlinx.serialization.Serializable
import tw_models.abilities.Ability
import tw_models.abilities.BoundedAbility
import tw_models.abilities.LoreOfMagic
import tw_models.abilities.Spell
import tw_models.addOrNull

@Serializable
data class Hero(
    override val name: String = "default_Hero",
    val raceList: List<String> = listOf(),
    val isLeg:Boolean = false,
    val recruitType: String = "building",
    val recruitSource: List<String> = listOf("quest_name", "dilemma_name", "building_name"),
    val mountList: List<String> = listOf(),
    override val charType: String = "",
    override val health: Int = 0,
    override val leadership: Int = 0,
    override val speed: Int = 0,
    override val meleeAttack: Int = 0,
    override val meleeDefense: Int = 0,
    override val chargeBonus: Int = 0,
    override val meleeWeaponStrength: Int = 0,
    override val meleeBaseDamage: Int = 0,
    override val meleeAP: Int = 0,
    override val meleeInterval: Double = 0.0,
    override val meleeReach: Int = 0,
    override val meleeVsBuilding: Double = 0.0,
    override val hasRanged: Boolean = false,
    override val rangedMissileStrength: Int = 0,
    override val rangedBaseDamage: Int = 0,
    override val rangedAp: Int = 0,
    override val rangedProjNum: Int = 0,
    override val rangedVsBuilding: Double = 0.0,
    override val reload: Int = 0,
    override val ammo: Int = 0,
    override val rangedRange: Int = 0,
    override val armor: Int = 0,
    override val gameID: String = "",
    override val upkeep: Int = 0,
    override val weightClass: String = "",
    override val costMP: Int = 0,
    override val missileBlock: Int = 0,
    override val abilityList: List<String> = listOf(),
    override val hasSpells: Boolean = false,
    override val availableLore: List<String> = listOf(),
    override val spellsMap: MutableMap<String, MutableList<String>> = mutableMapOf(),
    override val owner: String = "none"
)
    : Unit, Spellcaster

{
    override val unitCountSmall: Int = 1
    override val unitCountMedium: Int = 1
    override val unitCountLarge: Int = 1
    override val unitCountUltra: Int = 1
    val category: String = "Hero"
//    override fun addSpell(spell: String, lore: String) {
//        if (!this.hasSpells) throw Exception(this.name + " can not cast spells")
//        if (this.spellsMap.filterKeys { it == lore }.isEmpty() ) {
//            this.spellsMap[lore] = mutableListOf(spell)
//        }
//        else {
//            this.spellsMap[lore]?.add(spell)
//        }
//    }

//    fun raceListStr() : List<String> {
//        val raceList: MutableList<String> = mutableListOf()
//        for (i in 0 until this.raceList.size)
//            raceList.add(this.raceList[i])
//        return raceList.toList()
//    }
//
//    fun loreListStr() : List<String> {
//        val loreList: MutableList<String> = mutableListOf()
//        for (i in 0 until this.availableLore.size)
//            loreList.add(this.availableLore[i])
//        return loreList.toList()
//    }
//
//    fun ablListStr() : List<String> {
//        val ablList: MutableList<String> = mutableListOf()
//        for (i in 0 until this.abilityList.size)
//            ablList.add(this.abilityList[i].name)
//        return ablList.toList()
//    }
//
//    private fun spellsMapString():Map<String, List<String>> {
//        var spellMap : MutableMap<String, List<String>> = mutableMapOf()
//        for (key in this.spellsMap.keys.iterator()) {
//            val keyStr = key.name
//            val spellStr: MutableList<String> = mutableListOf()
//            val spellList = this.spellsMap[key]
//            if (spellList == null) continue
//            for (i in 0 until spellList.size)
//                spellStr.add(spellList[i].name)
//            spellMap[keyStr] = spellStr.toList()
//        }
//
//        return spellMap
//    }

//    fun toHeroData() :HeroData {
//        return HeroData (
//            name = this.name,
//            isLeg = this.isLeg,
//            raceList = this.raceList,
//            meleeBaseDamage = this.meleeBaseDamage,
//            recruitType = this.recruitType,
//            recruitSource = this.recruitSource,
//            mountList = this.mountList,
//            charType = this.charType,
//            health = this.health,
//            leadership = this.leadership,
//            meleeAttack = this.meleeAttack,
//            meleeReach = this.meleeReach,
//            meleeDefense = this.meleeDefense,
//            meleeInterval = this.meleeInterval,
//            meleeAP = this.meleeAP,
//            meleeWeaponStrength = this.meleeWeaponStrength,
//            meleeVsBuilding = this.meleeVsBuilding,
//            hasRanged = this.hasRanged,
//            rangedMissileStrength = this.rangedMissileStrength,
//            rangedRange = this.rangedRange,
//            rangedAP = this.rangedAp,
//            rangedProjNum = this.rangedProjNum,
//            rangedBaseDamage = this.rangedBaseDamage,
//            reload = this.reload,
//            rangeVsBuilding = this.rangedVsBuilding,
//            chargeBonus = this.chargeBonus,
//            availableLore = this.loreListStr(),
//            ammo = this.ammo,
//            gameID = this.gameID,
//            weightClass = this.weightClass,
//            upkeep = this.upkeep,
//            missileBlock = this.missileBlock,
//            abilityList = this.ablListStr(),
//            hasSpells = this.hasSpells,
//            armor = this.armor,
//            costMP = this.costMP,
//            spellsMap = this.spellsMapString(),
//            owner = this.owner
//        )
//    }
//
//    constructor(data: HeroData): this(
//        name = data.name,
//        raceList = if(data.raceList != null) raceFromString(data.raceList) else listOf(),
//        isLeg = addOrNull(data.isLeg, false),
//        recruitType = addOrNull(data.recruitType, ""),
//        recruitSource = addOrNull(data.recruitSource, listOf()),
//        mountList = addOrNull(data.mountList, listOf()),
//        charType = addOrNull(data.charType, ""),
//        health = addOrNull(data.health, 0),
//        leadership = addOrNull(data.leadership, 0),
//        meleeAttack = addOrNull(data.meleeAttack, 0),
//        meleeDefense = addOrNull(data.meleeDefense, 0),
//        chargeBonus = addOrNull(data.chargeBonus, 0),
//        meleeWeaponStrength = addOrNull(data.meleeWeaponStrength, 0),
//        meleeBaseDamage = addOrNull(data.meleeBaseDamage, 0),
//        meleeAP = addOrNull(data.meleeAP, 0),
//        meleeInterval = addOrNull(data.meleeInterval, 0.0),
//        meleeReach = addOrNull(data.meleeReach, 0),
//        meleeVsBuilding = addOrNull(data.meleeVsBuilding, 0.0),
//        hasRanged = addOrNull(data.hasRanged, false),
//        rangedMissileStrength = addOrNull(data.rangedMissileStrength, 0),
//        rangedBaseDamage = addOrNull(data.rangedBaseDamage, 0),
//        rangedAp = addOrNull(data.rangedAP, 0),
//        rangedProjNum = addOrNull(data.rangedProjNum, 0),
//        rangedVsBuilding = addOrNull(data.rangeVsBuilding, 0.0),
//        reload = addOrNull(data.reload, 0),
//        ammo = addOrNull(data.ammo, 0),
//        rangedRange = addOrNull(data.rangedRange, 0),
//        armor = addOrNull(data.armor, 0),
//        gameID = addOrNull(data.gameID, ""),
//        upkeep = addOrNull(data.upkeep, 0),
//        weightClass = addOrNull(data.weightClass, ""),
//        costMP = addOrNull(data.costMP, 0),
//        missileBlock = addOrNull(data.missileBlock, 0),
//        hasSpells = addOrNull(data.hasSpells, false),
//        owner = addOrNull(data.owner, "none"),
//        abilityList = addOrNull(data.abilityList, listOf()),
//
//
//
//
//    )


}
//fun <T>classFromStringList(strList: List<String>): List<T> {
//    var list: List<T> = listOf()
//    runBlocking {
//        if (T::class.simpleName == "Race") list = strList.map { Race(it) }.toList()
//        else if (T::class.simpleName == "Ability") list = strList.map { BoundedAbility(it) }.toList()
//    }
//    return list
//}
//
//
//fun raceFromString(strList: List<String>): List<Race> {
//    var list: List<Race> = listOf()
//    runBlocking {
//        list = strList.map { Race(it) }.toList()
//    }
//    return list
//}
//
//fun abilityFromString(strList: List<String>): List<Ability> {
//    var list: List<Ability> = listOf()
//    runBlocking {
//        list = strList.map { BoundedAbility(it) }.toList()
//    }
//    return list
//}
