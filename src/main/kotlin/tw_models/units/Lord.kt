package tw_models.units
import kotlinx.serialization.Serializable
import tw_models.abilities.LoreOfMagic
import tw_models.abilities.Spell
import tw_models.factions.Race

@Serializable
abstract class Lord: Unit, Spellcaster{
    abstract val race: String
    abstract val isLeg: Boolean
    abstract val mountList: List<String>

//    override fun addSpell(spell: Spell, lore: LoreOfMagic) {
//        if (!this.hasSpells) throw Exception(this.name + " can not cast spells")
//        if (this.spellsMap.filterKeys { it == lore }.isEmpty() ) {
//            this.spellsMap[lore] = mutableListOf(spell)
//        }
//        else {
//            this.spellsMap[lore]?.add(spell)
//        }
//    }
//
//
//
//    protected fun abilityListString():List<String> {
//        val ablList: MutableList<String> = mutableListOf()
//        for (i in 0 until this.abilityList.size)
//            ablList.add(this.abilityList[i].name)
//        return ablList.toList()
//    }
//
//    protected fun spellsMapString():Map<String, List<String>> {
//        val spellMap : MutableMap<String, List<String>> = mutableMapOf()
//        for (key in this.spellsMap.keys.iterator()) {
//            val keyStr = key.name
//            val spellStr: MutableList<String> = mutableListOf()
//            val spellList = this.spellsMap[key]
//            if (spellList == null) continue
//            for (i in 0 until spellList.size)
//                spellStr.add(spellList[i].name)
//            spellMap[keyStr] = spellStr.toList()
//        }
//
//        return spellMap
//    }
//
//    abstract fun toLordData() : LordData
}