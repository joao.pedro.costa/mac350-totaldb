package tw_models.units

interface BaseLord {
    val race: String
    val isLeg: Boolean
    val mountList: List<String>?
}