package tw_models.units

import tw_models.abilities.Ability

interface Unit {
    val name: String
    val charType: String?
    val health: Int?
    val leadership: Int?
    val speed: Int?
    val meleeAttack: Int?
    val meleeDefense: Int?
    val chargeBonus: Int?
    val meleeWeaponStrength: Int?
    val meleeBaseDamage: Int?
    val meleeAP: Int?
    val meleeInterval: Double?
    val meleeReach: Int?
    val meleeVsBuilding: Double?
    val hasRanged: Boolean?
    val rangedMissileStrength: Int?
    val rangedBaseDamage: Int?
    val rangedAp: Int?
    val rangedProjNum: Int?
    val rangedVsBuilding: Double?
    val reload: Int?
    val ammo: Int?
    val rangedRange: Int?
    val armor: Int?
    val gameID: String?
    val upkeep: Int?
    val weightClass: String?
    val costMP: Int?
    val missileBlock: Int?
    val unitCountSmall: Int?
    val unitCountMedium: Int?
    val unitCountLarge: Int?
    val unitCountUltra: Int?
    val abilityList: List<String>?
    val owner: String?


}