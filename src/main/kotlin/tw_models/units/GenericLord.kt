package tw_models.units

import tw_models.factions.Race
import kotlinx.serialization.Serializable
import tw_models.abilities.Ability
import tw_models.abilities.LoreOfMagic
import tw_models.abilities.Spell

@Serializable
class GenericLord(
    override val name: String = "default_GL",
    override val race: String = "",
    override val mountList: List<String> = listOf(),
    override val charType: String = "",
    override val health: Int = 0,
    override val leadership: Int = 0,
    override val speed: Int = 0,
    override val meleeAttack: Int = 0,
    override val meleeDefense: Int = 0,
    override val chargeBonus: Int = 0,
    override val meleeWeaponStrength: Int = 0,
    override val meleeBaseDamage: Int = 0,
    override val meleeAP: Int = 0,
    override val meleeInterval: Double = 0.0,
    override val meleeReach: Int = 0,
    override val meleeVsBuilding: Double = 0.0,
    override val hasRanged: Boolean = false,
    override val rangedMissileStrength: Int = 0,
    override val rangedBaseDamage: Int = 0,
    override val rangedAp: Int = 0,
    override val rangedProjNum: Int = 0,
    override val rangedVsBuilding: Double = 0.0,
    override val reload: Int = 0,
    override val ammo: Int = 0,
    override val rangedRange: Int = 0,
    override val armor: Int = 0,
    override val gameID: String = "",
    override val upkeep: Int = 0,
    override val weightClass: String = "",
    override val costMP: Int = 0,
    override val missileBlock: Int = 0,
    override val abilityList: List<String> = listOf(),
    override val hasSpells: Boolean = false,
    override val availableLore: List<String> = listOf(),
    override val spellsMap: MutableMap<String, MutableList<String>> = mutableMapOf(),
    override val owner: String
)
    : Lord()
{
    override val unitCountSmall: Int = 1
    override val unitCountMedium: Int = 1
    override val unitCountLarge: Int = 1
    override val unitCountUltra: Int = 1
    override val isLeg:Boolean = false

//    override fun toLordData(): LordData {
//        return LordData (
//            name= this.name,
//            charType = this.charType,
//            isPlayable = false,
//            race = this.race,
//            faction = "none",
//            startingArmy = null,
//            health = this.health,
//            leadership = this.leadership,
//            speed = this.speed,
//            meleeInterval = this.meleeInterval,
//            meleeAP = this.meleeAP,
//            meleeReach = this.meleeReach,
//            meleeDefense = this.meleeDefense,
//            mountList = this.mountList,
//            meleeAttack = this.meleeAttack,
//            meleeBaseDamage = this.meleeBaseDamage,
//            meleeWeaponStrength = this.meleeWeaponStrength,
//            costMP = this.costMP,
//            upkeep = this.upkeep,
//            weightClass = this.weightClass,
//            gameID = this.gameID,
//            abilityList = this.abilityList,
//            hasSpells = this.hasSpells,
//            spellsMap = this.spellsMap,
//            isLeg = false,
//            chargeBonus = this.chargeBonus,
//            owner = this.owner
//        )
//    }

}