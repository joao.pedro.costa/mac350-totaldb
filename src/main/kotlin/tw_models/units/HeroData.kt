package tw_models.units
import kotlinx.serialization.Serializable
import mongoConnection.MongoData
import tw_models.abilities.Ability

@Serializable
data class HeroData(
    override val name: String,
    val raceList: List<String>?,
    val isLeg: Boolean?,
    val recruitType: String?,
    val recruitSource: List<String>?,
    val mountList: List<String>?,
    val charType: String?,
    val health: Int?,
    val leadership: Int?,
    val meleeAttack: Int?,
    val meleeDefense: Int?,
    val chargeBonus: Int?,
    val meleeWeaponStrength: Int?,
    val meleeBaseDamage: Int?,
    val meleeAP: Int?,
    val meleeInterval: Double?,
    val meleeReach: Int?,
    val meleeVsBuilding: Double?,
    val hasRanged: Boolean?,
    val rangedMissileStrength: Int?,
    val rangedBaseDamage: Int?,
    val rangedAP: Int?,
    val rangedProjNum: Int?,
    val rangeVsBuilding: Double?,
    val reload: Int?,
    val ammo: Int?,
    val rangedRange: Int?,
    val  armor: Int?,
    val gameID: String?,
    val upkeep: Int?,
    val weightClass: String?,
    val costMP: Int?,
    val missileBlock: Int?,
    val abilityList: List<String>?,
    val hasSpells: Boolean?,
    val availableLore: List<String>?,
    val spellsMap: Map<String, List<String>>?,
    val owner: String?

): MongoData()