package tw_models.units

import kotlinx.serialization.Serializable
import tw_models.abilities.Ability
import tw_models.abilities.LoreOfMagic
import tw_models.abilities.Spell
import tw_models.factions.Race
import tw_models.factions.Faction

@Serializable
class LegendaryLord (
    override val name:String = "default_LL",
    val isPlayable: Boolean = true,
    override val race: String = "",
    val faction: String = "",
    val startingArmy: List<String> = listOf(),
    override val mountList: List<String> = listOf(),
    override val charType: String = "",
    override val health: Int = 0,
    override val leadership: Int = 0,
    override val speed: Int = 0,
    override val meleeAttack: Int = 0,
    override val meleeDefense: Int = 0,
    override val chargeBonus: Int = 0,
    override val meleeWeaponStrength: Int = 0,
    override val meleeBaseDamage: Int = 0,
    override val meleeAP: Int = 0,
    override val meleeInterval: Double = 0.0,
    override val meleeReach: Int = 0,
    override val meleeVsBuilding: Double = 0.0,
    override val hasRanged: Boolean = false,
    override val rangedMissileStrength: Int = 0,
    override val rangedBaseDamage: Int = 0,
    override val rangedAp: Int = 0,
    override val rangedProjNum: Int = 0,
    override val rangedVsBuilding: Double = 0.0,
    override val reload: Int = 0,
    override val ammo: Int = 0,
    override val rangedRange: Int = 0,
    override val armor: Int = 0,
    override val gameID: String = "",
    override val upkeep: Int = 0,
    override val weightClass: String = "",
    override val costMP: Int = 0,
    override val missileBlock: Int = 0,
    override val abilityList: List<String> = listOf(),
    override val hasSpells: Boolean = false,
    override val availableLore: List<String> = listOf(),
    override val spellsMap: MutableMap<String, MutableList<String>> = mutableMapOf(),
    override val owner: String?
)
    : Lord()
{
    override val unitCountSmall: Int = 1
    override val unitCountMedium: Int = 1
    override val unitCountLarge: Int = 1
    override val unitCountUltra: Int = 1
    override val isLeg:Boolean = true

//    private fun armyToListString():List<String> {
//        val armyList: MutableList<String> = mutableListOf()
//        for (i in 0 until this.startingArmy.size)
//            armyList.add(this.startingArmy[i].name)
//        return armyList.toList()
//    }
//    override fun toLordData(): LordData {
//        return LordData(
//            name = this.name,
//            charType = this.charType,
//            isPlayable = this.isPlayable,
//            race = this.race,
//            faction = this.faction,
//            startingArmy = this.startingArmy,
//            health = this.health,
//            leadership = this.leadership,
//            speed = this.speed,
//            meleeAttack = this.meleeAttack,
//            meleeDefense = this.meleeDefense,
//            chargeBonus = this.chargeBonus,
//            meleeWeaponStrength = this.meleeWeaponStrength,
//            meleeBaseDamage = this.meleeBaseDamage,
//            meleeAP = this.meleeAP,
//            meleeInterval = this.meleeInterval,
//            meleeReach = this.meleeReach,
//            costMP = this.costMP,
//            upkeep = this.upkeep,
//            weightClass = this.weightClass,
//            gameID = this.gameID,
//            mountList = this.mountList,
//            abilityList = this.abilityList,
//            hasSpells = this.hasSpells,
//            isLeg = true,
//            spellsMap = this.spellsMap,
//            owner = this.owner
//        )
//    }
}




