package tw_models.units

import tw_models.abilities.LoreOfMagic
import tw_models.abilities.Spell

interface Spellcaster {
    val hasSpells: Boolean?
    val availableLore: List<String>?
    val spellsMap: MutableMap<String, MutableList<String>>?

//    fun addSpell(spell: String, lore: String)
}