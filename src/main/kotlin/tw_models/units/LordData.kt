package tw_models.units

import kotlinx.serialization.Serializable
import mongoConnection.MongoData
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import tw_models.addOrNull
import java.util.Date

@Serializable
data class LordData(
    val id: String? = null,
    override val name: String,
    val charType: String? = "",
    val isPlayable: Boolean? = true,
    val race: String? = "",
    val faction: String? = "",
    val startingArmy: List<String>? = listOf(),
    val health: Int? = 0,
    val leadership: Int? = 0,
    val speed: Int? = 0,
    val meleeAttack: Int? = 0,
    val meleeDefense: Int? = 0,
    val chargeBonus: Int? = 0,
    val meleeWeaponStrength: Int? = 0,
    val meleeBaseDamage: Int? = 0,
    val meleeAP: Int? = 0,
    val meleeInterval: Double? = 0.0,
    val meleeReach: Int? = 0,
    val costMP: Int? = 0,
    val upkeep: Int? = 0,
    val weightClass: String? = "",
    val gameID: String? = "",
    val mountList: List<String>?= listOf(),
    val abilityList: List<String>? = listOf(),
    val hasSpells: Boolean? = false,
    val spellsMap: Map<String, List<String>>? = mapOf(),
    val isLeg: Boolean = false,
    val createdAt: String? = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")),
    var owner: String?,
    val armor: Int? = 0,
    val meleeVsBuilding: Double? = 1.0,
    val hasRanged: Boolean? = false,
    val missileBlock: Int? = 0
) : MongoData(){
    val unitCountSmall: Int = 1
    val unitCountMedium: Int = 1
    val unitCountLarge: Int = 1
    val unitCountUltra: Int = 1
    val category: String = "Lord"

    fun toLegendaryLord() : LegendaryLord{
        return LegendaryLord(
            name = this.name,
            charType = addOrNull(this.charType, ""),
            isPlayable = addOrNull(this.isPlayable,false),
            race = addOrNull(this.race, ""),
            faction = addOrNull(this.faction, ""),
            startingArmy = addOrNull(this.startingArmy, listOf()),
            health = addOrNull(this.health, 0),
            leadership = addOrNull(this.leadership, 0),
            speed = addOrNull(this.speed, 0),
            meleeAttack = addOrNull(this.meleeAttack, 0),
            meleeDefense = addOrNull(this.meleeDefense, 0),
            chargeBonus = addOrNull(this.chargeBonus, 0),
            meleeWeaponStrength = addOrNull(this.meleeWeaponStrength, 0),
            meleeAP = addOrNull(this.meleeAP, 0),
            meleeInterval = addOrNull(this.meleeInterval, 0.0),
            meleeReach = addOrNull(this.meleeReach, 0),
            costMP = addOrNull(this.costMP, 0),
            upkeep = addOrNull(this.upkeep, 0),
            weightClass = addOrNull(this.weightClass,""),
            gameID = addOrNull(this.gameID,""),
            mountList = addOrNull(this.mountList, listOf()),
            abilityList = addOrNull(this.abilityList, listOf()),
            hasSpells = addOrNull(this.hasSpells,false),
            owner = addOrNull(this.owner,""))
    }

    fun toGenericLord() : GenericLord{
        return GenericLord(

            name= this.name,
            charType = addOrNull(this.charType,""),
            race = addOrNull(this.race,""),
            health = addOrNull(this.health,0),
            leadership = addOrNull(this.leadership,0),
            speed = addOrNull(this.speed,0),
            meleeInterval = addOrNull(this.meleeInterval,0.0),
            meleeAP = addOrNull(this.meleeAP,0),
            meleeReach = addOrNull(this.meleeReach,0),
            meleeDefense = addOrNull(this.meleeDefense,0),
            mountList = addOrNull(this.mountList,listOf()),
            meleeAttack = addOrNull(this.meleeAttack,0),
            meleeWeaponStrength = addOrNull(this.meleeWeaponStrength,0),
            costMP = addOrNull(this.costMP,0),
            upkeep = addOrNull(this.upkeep,0),
            weightClass = addOrNull(this.weightClass,""),
            gameID = addOrNull(this.gameID,""),
            abilityList = addOrNull(this.abilityList,listOf()),
            hasSpells = addOrNull(this.hasSpells,false),
            chargeBonus = addOrNull(this.chargeBonus,0),
            owner = addOrNull(this.owner,"")
        )
    }

}

