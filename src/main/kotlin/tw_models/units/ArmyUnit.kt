package tw_models.units


import kotlinx.serialization.Serializable
import tw_models.factions.Race
import tw_models.abilities.Ability

@Serializable
data class ArmyUnit(
    override val name: String = "default unit",
    val category: String,
    val isLeg: Boolean? = false,
    val recruitTypes: List<String>? = listOf(),
    val recruitSources: List<String>? = listOf(),
    val race: String? = "",
    override val charType: String? = "",
    override val health: Int? = 0,
    override val leadership: Int? = 0,
    override val speed: Int? = 0,
    override val meleeAttack: Int? = 0,
    override val meleeDefense: Int? = 0,
    override val chargeBonus: Int? = 0,
    override val meleeWeaponStrength: Int? = 0,
    override val meleeBaseDamage: Int? = 0,
    override val meleeAP: Int? = 0,
    override val meleeInterval: Double? = 0.0,
    override val meleeReach: Int? = 0,
    override val meleeVsBuilding: Double? = 0.0,
    override val hasRanged: Boolean? = false,
    override val rangedMissileStrength: Int? = 0,
    override val rangedBaseDamage: Int? = 0,
    override val rangedAp: Int? = 0,
    override val rangedProjNum: Int? = 0,
    override val rangedVsBuilding: Double? = 0.0,
    override val reload: Int? = 0,
    override val ammo: Int? = 0,
    override val rangedRange: Int? = 0,
    override val armor: Int? = 0,
    override val gameID: String? = "",
    override val upkeep: Int? = 0,
    override val weightClass: String? = "",
    override val costMP: Int? = 0,
    override val missileBlock: Int? = 0,
    override val unitCountSmall: Int? = 0,
    override val unitCountMedium: Int? = 0,
    override val unitCountLarge: Int? = 0,
    override val unitCountUltra: Int? = 0,
    override val abilityList: List<String>? = listOf(),
    override val owner: String? = "none",
): Unit