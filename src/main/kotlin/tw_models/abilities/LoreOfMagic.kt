package tw_models.abilities

import tw_models.factions.Race
import kotlinx.serialization.Serializable
import mongoConnection.MongoData
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.codecs.pojo.annotations.BsonProperty

@Serializable
data class LoreOfMagic (
    @BsonProperty("_id") val id: String? = null,
    override val name: String = "",
    val availableFactions: List<String> = listOf(),
    val spells: List<String> = listOf(),
    val owner: String = "none"
) : MongoData()
{
}