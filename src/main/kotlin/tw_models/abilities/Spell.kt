package tw_models.abilities
import kotlinx.serialization.Serializable

@Serializable
data class Spell(
    override val name: String = "",
    override val ablType: String? = "",
    override val useCount: Int? = 0,
    override val duration: Int? = 0,
    override val cooldown: Int? = 0,
    override val effects: List<String>? = listOf(),
    override val range: Int? = 0,
    override val effectRange: Int? = 0,
    override val affectedUnits: String? = "",
    override val affectsIf: String? = "",
    val magicLore: String? = "",
    val targets: List<String>? = listOf(),
    val windsCost: Int? = 0,
    val overcastCost: Int? = 0,
    val overcastEffects: List<String>? = listOf(),
    override val owner: String? = "none"
) : Ability() {

}
