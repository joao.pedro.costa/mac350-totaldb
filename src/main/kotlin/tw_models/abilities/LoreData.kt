package tw_models.abilities
import kotlinx.serialization.Serializable
import mongoConnection.MongoData

@Serializable
data class LoreData(
    override val name: String,
    val availableFactions: List<String>?,
    val spells: List<String>?,
    val owner: String?
): MongoData()