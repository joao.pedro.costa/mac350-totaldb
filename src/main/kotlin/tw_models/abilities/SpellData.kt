package tw_models.abilities
import kotlinx.serialization.Serializable
import mongoConnection.MongoData

@Serializable
data class SpellData (
    override val name: String,
    val ablType: String?,
    val useCount: Int?,
    val duration: Int?,
    val cooldown: Int?,
    val range: Int?,
    val effectiveRange: Int?,
    val effects: List<String>?,
    val affectedUnits: String?,
    val affectsIf: String?,
    val magicLore: String?,
    val windsCost: Int?,
    val overcastCost: Int?,
    val targets: List<String>?,
    val overcastEffects: List<String>?,
    val owner: String?
): MongoData()