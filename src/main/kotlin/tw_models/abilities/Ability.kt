package tw_models.abilities
import kotlinx.serialization.Serializable

@Serializable
abstract class Ability {
    abstract val name: String?
    abstract val ablType: String?
    abstract val useCount: Int?
    abstract val duration: Int?
    abstract val cooldown: Int?
    abstract val effects: List<String>?
    abstract val range: Int?
    abstract val effectRange: Int?
    abstract val affectedUnits: String?
    abstract val affectsIf: String?
    abstract val owner: String?
}