package tw_models

fun <T>addOrNull(field: T?, defaultValue: T) : T {
    if (field == null) return defaultValue else return field
}