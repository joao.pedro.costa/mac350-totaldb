package tw_models.factions
import kotlinx.serialization.Serializable
import tw_models.units.LegendaryLord
import tw_models.units.LordData

@Serializable
data class Faction(
    val name: String = "defaultSubFactionName",
    val race: String = "",
    val isMajor: Boolean = false,
    val owner: String = "none"
)

{

    var startingLord: LordData = LordData(name = "default_LL", race = this.race, faction = this.name, owner = "none")
        set(value) {
            if (field.name == "default_LL" ) field = value
        }


//    constructor(data: FactionData): this(
//        name = data.name,
//        race = if (data.race != null) Race(data.race) else Race(),
//        isMajor = if(data.isMajor != null) data.isMajor else false,
//        owner = if(data.owner != null) data.owner else "none"
//    )

}