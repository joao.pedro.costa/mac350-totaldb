package tw_models.factions

import kotlinx.serialization.Serializable

@Serializable
data class Race(
    val name: String = "defaultFactionName",
    val owner: String = "none"
) {

    private var subFactionsCount = 0
    private var majorFactions: MutableList<Faction> = mutableListOf()
    private var minorFactions: MutableList<Faction> = mutableListOf()

    fun includeSubFaction(sFaction: Faction) {
        this.subFactionsCount += 1
        if (sFaction.isMajor)
            this.majorFactions.add(sFaction)
        else
            this.minorFactions.add(sFaction)
    }

    fun getMajorFactions(): List<Faction> {
        return this.majorFactions
    }

    fun getMinorFactions(): List<Faction> {
        return this.minorFactions
    }

    fun getAllFactions(): List<Faction> {
        var factions: MutableList<Faction> = ArrayList()
        for (faction in this.majorFactions)
            factions.add(faction)
        for (faction in this.minorFactions)
            factions.add(faction)
        return factions
    }

    fun printAllSubFactions() {
        println("Legendary Factions:")
        for (faction in this.majorFactions)
            println(faction.name)
        println("Minor factions:")
        for (faction in this.minorFactions)
            println(faction.name)
    }

    constructor(data: RaceData) : this(
        name = data.name,
        owner = if(data.owner != null) data.owner else "none"
    )
}