package tw_models.factions

import kotlinx.serialization.Serializable
import mongoConnection.MongoData
import javax.print.attribute.standard.DialogOwner

@Serializable
data class RaceData(
    override val name: String,
    val subFactionCount: Int?,
    val majorFactions: List<String>?,
    val minorFactions: List<String>?,
    val owner: String?
) : MongoData()