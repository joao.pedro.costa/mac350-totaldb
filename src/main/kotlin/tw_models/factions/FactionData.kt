package tw_models.factions

import kotlinx.serialization.Serializable
import mongoConnection.MongoData
@Serializable
data class FactionData(
    override val name: String,
    val race: String?,
    val isMajor: Boolean?,
    val startingLord: String?,
    val owner: String?
): MongoData()