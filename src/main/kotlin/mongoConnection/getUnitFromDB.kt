package mongoConnection
//
//import com.google.gson.Gson
//import kotlinx.coroutines.runBlocking
//import org.bson.Document
//import tw_models.units.ArmyUnit
//import com.mongodb.client.model.Filters.eq
//import kotlinx.coroutines.flow.firstOrNull
//import tw_models.units.UnitData
//
//fun getUnitFromDB(name:String?) : ArmyUnit? {
//    var unit: ArmyUnit? = ArmyUnit("default")
//    if (name != null) {
//        runBlocking {
//            val response: UnitData? = unitCollection.find(eq("name", name)).firstOrNull()
//            if (response == null) unit = null
//            else {
////            val responseJson = response.toJson()
////            val unitData: UnitData = Gson().fromJson(responseJson, UnitData::class.java)
//                unit = ArmyUnit(response.name)
//            }
//        }
//    }
//    return unit
//}
//
//fun getUnitFromDB(name: List<String>?) : List<ArmyUnit> {
//    var unitList = mutableListOf<ArmyUnit>()
//    if (name != null) {
//        for (i in 0 until name.size) {
//            val newUnit = getUnitFromDB(name[i])
//            if (newUnit != null) unitList.add(newUnit)
//        }
//    }
//    return unitList.toList()
//}