package mongoConnection

import com.mongodb.client.model.Filters.all
import com.mongodb.client.model.Filters.eq
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import tw_models.abilities.LoreOfMagic
import tw_models.abilities.Spell
import tw_models.factions.Faction
import tw_models.factions.FactionData
import tw_models.units.*

//fun getHeroesFromDB(name: String?) : Hero? {
//    var hero: Hero? = Hero("default")
//    if (name != null) {
//        runBlocking {
//            val response: HeroData? = heroesColletion.find(eq("name", name)).firstOrNull()
//            if (response == null) hero = null
//            else {
//                hero = Hero(
//                    name = response.name,
//                    hasSpells = if(response.hasSpells != null) response.hasSpells else false,
//                    spellsMap = makeSpellMap(response.spellsMap)
//                )
//            }
//
//        }
//    }
//    return hero
//}

//fun getHeroesFromDB(name: List<String>?): List<Hero?> {
//    var heroList = mutableListOf<Hero?>()
//    if (name != null) {
//        for (i in 0 until name.size)
//            heroList.add(getHeroesFromDB(name[i]))
//    }
//    return heroList.toList()
//}
//fun getHeroesFromUser(user : String?) : List<Hero> {
//    var list: List<Faction> = listOf()
//    var response = heroesColletion.find<Hero>(all("owner", listOf(user, "global")))
//    runBlocking {
//        list = response.map {
//            Faction(name = it.name, isMajor = if (it.isMajor != null) it.isMajor else false)
//        }.toList()
//    }
//    return list
//}