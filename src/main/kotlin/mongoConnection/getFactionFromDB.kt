package mongoConnection

import tw_models.factions.Faction

import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.Filters.all
import com.mongodb.client.model.Filters.or
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import tw_models.factions.FactionData
import tw_models.units.*

//fun getFactionFromDB(name: String?) : Faction? {
//    var faction: Faction? = Faction("default")
//    if (name != null) {
//        runBlocking {
//            val response: Faction? = factionsCollection.find(eq("name", name)).firstOrNull()
//            if (response == null) faction = null
//            else {
//                faction = Faction(
//                    name = response.name,
//                    isMajor = if(response.isMajor != null) response.isMajor else false,
//                )
//            }
//
//        }
//    }
//
//    return faction
//}
//
//fun getFactionFromDB(name: List<String>?): List<Faction?> {
//    var factionList: List<Faction> = listOf()
//    if (name != null) {
//        runBlocking { factionList = factionsCollection.find<Faction>(all("name", name)).toList()}
//    }
//
//    return factionList
//}
//
//fun getFactionsFromUser(user : String?) : List<Faction> {
//    var factionList: List<Faction> = listOf()
//    val factionResponse = factionsCollection.find<Faction>(all("owner", listOf(user, "global")))
//    runBlocking {
//        factionList = factionResponse.toList()
//    }
//    return factionList
//}
