package mongoConnection

import kotlinx.serialization.Serializable
import com.mongodb.client.model.Filters.eq
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.runBlocking
import org.bson.Document
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.codecs.pojo.annotations.BsonIgnore
import org.bson.types.ObjectId

//
//@Serializable
//class AuthData (@BsonIgnore val id: Int? = null, val usr: String, val pass: String)

var authDB = totalDBClient.getDatabase("Authentication")
var authDev = authDB.getCollection<Document>("dev")

public fun findUserPass(user: String): String? {
    var pass: String? = null
    runBlocking {
        val auth: Document? = authDev.find(eq("usr", user)).firstOrNull()

        if (auth != null) {
            auth.let {
                pass = it.getString("pass")
            }
    }
//            val auth = authDev.find<AuthData>(eq("usr", user)).firstOrNull()
//
//            if (auth != null) pass = auth.pass

    }
    return pass
}

public fun createUser(user: String, pass: String): Boolean {
    var validateCreation = false
    runBlocking {
        var validateUsr = authDev.find(eq("usr", user)).firstOrNull()

        if (validateUsr == null) {
            val newUsr = Document("_id", ObjectId())
                .append("usr", user)
                .append("pass", pass)

            authDev.insertOne(newUsr)
            validateCreation = true
        }
    }
    return validateCreation
}
//
public fun updatePass(user: String, oldPass: String, newPass: String) :Boolean {
    var validateUpdate = false
    runBlocking {
        val validateUsr = authDev.find(eq("usr", user)).firstOrNull()
        if (validateUsr != null){
            val pass = validateUsr.getString("pass")
            if (pass == oldPass){
                val newUsr = Document("_id", ObjectId())
                    .append("usr", user)
                    .append("pass", newPass)

                authDev.replaceOne(eq("usr", user), newUsr)
                validateUpdate = true
            }

        }
    }
    return validateUpdate
}

