package mongoConnection
import com.mongodb.connection.SocketSettings
import com.mongodb.kotlin.client.coroutine.MongoClient
import org.bson.Document
import tw_models.abilities.LoreData
import tw_models.abilities.LoreOfMagic
import tw_models.abilities.Spell
import tw_models.factions.Faction
import tw_models.factions.RaceData
import tw_models.units.Hero
import tw_models.units.LordData
import tw_models.units.ArmyUnit
import java.util.concurrent.TimeUnit

var connectStr = "mongodb+srv://joaopedrocosta:y41pos86nUm51Xdz@cluster0.tavba3c.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0"

public var totalDBClient = MongoClient.create(connectStr)
public var totalDatabase = totalDBClient.getDatabase("TotalDB-dev")
public var lordsCollection = totalDatabase.getCollection<LordData>("lords")
public var heroesColletion = totalDatabase.getCollection<Hero>("heroes")
public var factionsCollection = totalDatabase.getCollection<Faction>("factions")
public var racesCollection = totalDatabase.getCollection<RaceData>("races")
public var loresCollection = totalDatabase.getCollection<LoreData>("lore-of-magic")
public var spellsCollection = totalDatabase.getCollection<Spell>("spells")
public var unitCollection = totalDatabase.getCollection<ArmyUnit>("army-unit")