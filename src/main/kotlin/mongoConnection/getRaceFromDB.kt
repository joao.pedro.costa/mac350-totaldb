package mongoConnection
//
//
//import com.mongodb.client.model.Filters.all
//import com.mongodb.client.model.Filters.eq
//import kotlinx.coroutines.flow.firstOrNull
//import kotlinx.coroutines.flow.map
//import kotlinx.coroutines.flow.toList
//import kotlinx.coroutines.runBlocking
//import tw_models.factions.Faction
//import tw_models.factions.FactionData
//import tw_models.factions.Race
//import tw_models.factions.RaceData
//import tw_models.units.*
//
//fun getRaceFromDB(name: String?) : Race? {
//    var race: Race? = Race("default")
//    if (name != null) {
//        runBlocking {
//            val response: RaceData? = racesCollection.find(eq("name", name)).firstOrNull()
//            if (response == null) race = null
//            else {
//                race = Race(
//                    name = response.name,
//                )
//            }
//
//        }
//    }
//
//    return race
//}
//
//fun getRaceFromDB(name: List<String>?): List<Race?> {
//    var raceList = mutableListOf<Race?>()
//    if (name != null) {
//        for (i in 0 until name.size)
//            raceList.add(getRaceFromDB(name[i]))
//    }
//    return raceList.toList()
//}
//
//fun getRacesFromUser(user : String?) : List<Race> {
//    var list: List<Race> = listOf()
//    var response = racesCollection.find<RaceData>(all("owner", listOf(user, "global")))
//    runBlocking {
//        list = response.map {
//            Race(it)
//        }.toList()
//    }
//    return list
//}
