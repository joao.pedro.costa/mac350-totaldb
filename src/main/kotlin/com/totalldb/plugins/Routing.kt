package com.totaldb.plugins

import com.mongodb.client.model.Filters
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import com.mongodb.client.model.Filters.*
import com.mongodb.client.model.Projections
import com.mongodb.kotlin.client.coroutine.MongoCollection
import io.ktor.server.auth.*
import io.ktor.server.util.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.withTimeoutOrNull
import mongoConnection.*
import tw_models.abilities.LoreData
import tw_models.abilities.Spell
import tw_models.factions.Race
import tw_models.factions.RaceData
import tw_models.units.*
import java.util.logging.Filter

fun Application.configureRouting() {
    routing {

        get("/") {
            call.respond(HttpStatusCode.OK)
        }

        get( "/db/katarin"){
            val katarin = lordsCollection.find<LordData>(eq("name", "Tzaarina Katarin")).firstOrNull()
            if (katarin != null)  call.respondText{katarin.name}
            else call.respondText { "Katarin not found" }
        }
        get ("/db/getAllRaces"){

            val results = racesCollection.find(empty())
            val races = results.toList()
            val names = races.map {it.name}
            call.respond(names)
        }

        post("/db/addUnit") {
            val newUnit:ArmyUnit = call.receive<ArmyUnit>()
            val unitOnDB: ArmyUnit? = unitCollection.find<ArmyUnit>(eq("name",newUnit.name)).firstOrNull()
            if (unitOnDB == null) {
                unitCollection.insertOne(newUnit)
            }
            else {
                if (unitOnDB.owner == newUnit.owner) {
                    unitCollection.replaceOne(eq("name", newUnit.name), newUnit)
                }
            }
            call.respondText { "ok" }
        }


        post("/db/addHero") {
            val newUnit:Hero = call.receive<Hero>()
            val unitOnDB: Hero? = heroesColletion.find<Hero>(eq("name",newUnit.name)).firstOrNull()
            if (unitOnDB == null) {
                heroesColletion.insertOne(newUnit)
            }
            else {
                if (unitOnDB.owner == newUnit.owner) {
                    heroesColletion.replaceOne(eq("name", newUnit.name), newUnit)
                }
            }
            call.respondText { "ok" }
        }

        post("/db/addLord") {
            val newLord:LordData = call.receive<LordData>()
            val lordOnDB: LordData? = lordsCollection.find<LordData>(eq("name",newLord.name)).firstOrNull()
            if (lordOnDB == null) {
                lordsCollection.insertOne(newLord)
            }
            else {
                if (lordOnDB.owner == newLord.owner) {
                    lordsCollection.replaceOne(eq("name", newLord.name), newLord)
                }
            }
            call.respondText { "ok" }
        }

        authenticate("Basic") {


        }

        get ("/db/getAllByRace/{race}/{type}/{user}") {
            val race: String? = call.parameters["race"]
            val dataType: String? = call.parameters["type"]
            val user: String? = call.parameters["user"]

            if (race == null || dataType == null || user == null) call.respond(HttpStatusCode.BadRequest)
            else {
                val ownerFilter = or(eq("owner", user), eq("owner", "global"))
                val raceFilter = eq("race", race)
                val heroRaceFilter = Filters.`in`("raceList", race)

                when (dataType) {
                    "lords" -> {
                        val results = lordsCollection.find<LordData>(and(raceFilter, ownerFilter))
                        val toRespond: List<LordData> = results.toList()
                        call.respond(toRespond)
                    }
                    "heroes" -> {
                        val results = heroesColletion.find<Hero>(and(heroRaceFilter, ownerFilter))
                        val toRespond: List<Hero> = results.toList()
                        call.respond(toRespond)
                    }
                    "spells" -> {
                        val results = spellsCollection.find<Spell>(and(raceFilter, ownerFilter))
                        val toRespond: List<Spell> = results.toList()
                        call.respond(toRespond)
                    }
                    "lores" -> {
                        val results = loresCollection.find<LoreData>(and(raceFilter, ownerFilter))
                        val toRespond: List<LoreData> = results.toList()
                        call.respond(toRespond)
                    }
                    else -> {
                        val results = unitCollection.find<ArmyUnit>(and(and(raceFilter, ownerFilter), eq("category", dataType)))
                        val toRespond: List<ArmyUnit> = results.toList()
                        call.respond(toRespond)

                    }
                }

            }
        }


        get ("db/getAllSpells/{lore}") {
            val lore: String? = call.parameters["lore"]
            if (lore == null) call.respond(HttpStatusCode.BadRequest)
            else {
                val results = spellsCollection.find<Spell>(eq("magicLore", lore))
                val toRespond = results.toList()
                call.respond(toRespond)
            }

        }

        get ("/db/getAllByUser/{user}/{type}") {
            val user: String? = call.parameters["user"]
            val dataType: String? = call.parameters["type"]

            if (user == null || dataType == null) call.respond(HttpStatusCode.BadRequest)

            else {
                val filer = eq("owner", user)
                when (dataType) {
                    "lords" -> {
                        val results = lordsCollection.find<LordData>(filer)
                        val toRespond: List<LordData> = results.toList()
                        call.respond(toRespond)
                    }
                    "heroes" -> {
                        val results = heroesColletion.find<Hero>(filer)
                        val toRespond: List<Hero> = results.toList()
                        call.respond(toRespond)
                    }
                    "spells" -> {
                        val results = spellsCollection.find<Spell>(filer)
                        val toRespond: List<Spell> = results.toList()
                        call.respond(toRespond)
                    }
                    "lores" -> {
                        val results = loresCollection.find<LoreData>(filer)
                        val toRespond: List<LoreData> = results.toList()
                        call.respond(toRespond)
                    }
                    else -> unitCollection
                }

            }
        }


        get ("/createUser/{usr}/{pass}"){
            val user: String? = call.parameters["usr"]
            val pass: String? = call.parameters["pass"]
            if (user != null && pass != null) {
                createUser(user, pass)
                call.respondText("User created", ContentType.Text.Plain, HttpStatusCode.OK)
            }
            else {
                call.respondText("error creating user ", ContentType.Text.Plain ,HttpStatusCode(201, "user not created"))
            }
        }

        get("/login/{usr}/{pass}") {
            val user: String? = call.parameters["usr"]
            val pass: String? = call.parameters["pass"]
            if (user != null && pass != null) {
                if (findUserPass(user) == pass) call.respond(HttpStatusCode.OK)
                else call.respond(HttpStatusCode.BadRequest)
            }
            else {
                call.respond(HttpStatusCode.BadRequest)
            }
        }
    }
}
