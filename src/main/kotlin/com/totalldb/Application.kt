package com.totaldb

import com.totaldb.plugins.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.plugins.defaultheaders.*
import io.ktor.server.response.*
import mongoConnection.findUserPass
import io.ktor.server.plugins.cors.*
//import io.ktor.server.plugins.cors.CORS
import io.ktor.server.plugins.cors.routing.CORS
import mongoConnection.totalDBClient
import org.w3c.dom.Document


fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)

}

fun Application.module() {
    configureSerialization()
    install(Authentication) {
        basic("Basic") {
            realm = "Access to the '/db/' path"
            validate { credentials ->
                val validatePass: String? = findUserPass(credentials.name)
                if (validatePass != null && credentials.password == validatePass) {
                    UserIdPrincipal(credentials.name)
                } else {
                    null
                }
            }
        }
    }
    install(CORS) {
        anyHost()
        allowHeader(HttpHeaders.ContentType)
        allowHeader(HttpHeaders.Authorization)
        allowMethod(HttpMethod.Get)
        allowMethod(HttpMethod.Post)
        allowMethod(HttpMethod.Options)
        allowSameOrigin
    }
    install(DefaultHeaders) {
        header(HttpHeaders.AccessControlAllowOrigin, "*")
    }
//    configureDatabases()
    configureRouting()
    println("TotalDB started")

}


