# MAC350-TotalDB

Um guia de referência para unidades, habilidades e facções do jogo Total War Warhammer 3. 

## Instalação
### Servidor DB:
Abrir o projecto no Intellij Idea. Caso necessário, atualizar o gradle, e dar build no arquivo /src/main/kotlin/com/totalldb/Application.kt
A conexão com o banco do MongoDB é feita automaticamente.

### Front End:
Arquivos do front se encontram no repositório:
https://gitlab.uspdigital.usp.br/joao.pedro.costa/totaldb-front
Depois de clonar o repositório, as dependencias podem ser instaladas com o comando
`npm install`
Depois das dependencias installadas, o serivdor do front pode ser criado com o comando
`npm run dev`



## Uso
O front será hospedado em `localhost:3000`, e o servidor do banco de dados será hospedado em `localhost:8080`. Abrindo http://localhost:3000 irá conectar ao site.

O site utiliza cookies para guardar informações de usuário e tema de cores.


### Tool bar
A barra superior do site, presente na maioria das páginas possui quatro funções principais: 
- clicar no ícone TotalDB irá redirecionar para a página principal
- clicar em **Spells** redireciona para a página de magias
- clicar nos ícones dos deuses de Warhammer modifica o tema do site para as cores que representam cada deuses
- menu de login, que permite loggar, criar uma conta, ou utilziar as funções de usuário quando logado.

### Página Principal
Observação: será utilizado o termo "raça", para representar as diferentes origens das unidades. Alguns lugares preferem utilizar "nação", mas nenhum dos dois termos corretamente engloba o que cada um representa.

Clicar em qualquer uma dos ícones das raças disponíveis irá redirecionar para a página com as unidads das raças. 

### Página das Raças
A lista à esquerda apresenta as categorias de unidades que podem ser apresentadas. Marcar ou desmarcar uma categoria altera o que é mostrado na tabela.

A tabela apresenta os dados das unidades com os tipos selecionados. Caso um usuário esteja loggado, também serão apresentadas as unidades customizadas daquele usuário. 

A tabela permite filtrar e ordenar as linhas, além de trocar a ordem das colunas.

### Página de Magias
Similar à pagina das Raças, porém escolhendo as escolas (Lore) de magia que serão apresentadas. As escolas de magia independem da raça.

### Funcioinalidades de Usuário
A página de Sign Up cria uma conta que será salva no MongoDB. Após criada a conta, pode-se utilizar as credenciais na página de Sign In para loggar na conta. Após loggado, os botões de Sign In e Sign Up serão trocador para um avatar com a inicial do usuário, criando um menu para as funcionalidades de usuário.

No momento, não é possível recuperar uma senha perdida diretamente pelo site. Caso necessário, entre em contato pelo email de suporte. 

O menu de usuário apresenta as seguintes funcionalidades:
- Criar Unidade: redireciona para uma página para criação de unidades customizadas. Basta preencher o formulário, e clicar em `Create Unit` para enviar uma nova unidade para o banco. Esta unidade ficará atrelada ao usuário que a criou. Caso queira atualizar uma unidade existente, basta reenviar o formulário com o mesmo nome de criação. No momento, não é possível deletar uma unidade criada.
- Minhas Unidades: redireciona para uma página similar à página de raças, mas mostrando somente as unidades criadas pelo usuário. São apresentadas unidades de todas as raças.
- Logout: deslogga o usuário atual e redireciona para a página principal.


## Suporte
Em caso de problemas, sugestões e dúvidas, entre em contato com joao.pedro.costa@usp.br

## Roadmap
 - Implementação das classes básicas de unidades e habilidades
 - Implementação de Kislev como facção de teste
 - And more

## Contribuições
Este projeto é uma tarefa para a disciplina MAC0350, assim não estamos aceitando contribuições de terceiros. 

## Autores e Reconhecimentos
Projeto desenvolvido por João Pedro Petrosino Costa para a disciplina MAC 0350 do Instituto de Matemática e Estatística da USP, sob orientação do professor Paulo Roberto Miranda Meirelles.

## Licença 
Projeto sob a licença GNU GENERAL PUBLIC LICENSE Version 3,

## Status do Projeto
Desenvolvimento em pré-alpha 
